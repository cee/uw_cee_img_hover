# Uwaterloo CEE Image Hover Shadow
This module adds a wysiwyg plug-in that enables adding a box-shadow around images(that redirect).

#### Setup
1. Enable Hover Shadow wysiwyg plug-in

#### Use
1. Select image by clicking image you want to edit in the wysiwyg editor(a drag box should appear around it)
2. Click on the Hover Shadow icon. ![icon](plugins/img_hover/image_hover_shadow.png)
3. If successful, the image should now have a static shadow in the editor (this will be changed to a hover-shadow once in preview mode)