/**
 * @file
 */

(function ($) {
    Drupal.behaviors.uw_cee_img_hover= {
        attach: function (context, settings) {
            //check if class exists
            if ($('.image_hover_shadow').length) {
                //changing css on hover
                $('.image_hover_shadow').hover(function () {
                    $(this).css({
                        'box-shadow': "0px 6px 15px rgba(0, 0, 0, 0.90)",
                        'transition': 'box-shadow 0.2s'
                    });
                }, function(){
                    $(this).css({
                        'box-shadow': "none",
                        'transition': 'box-shadow 0.2s'
                    });
                });
            }
        }
    };
})(jQuery);