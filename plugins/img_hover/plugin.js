/**
 * @file
 * Custom uWaterloo CKEditor buttons for image hover.
 */

(function () {

    function onSelectionChange(evt) {

        if (evt.editor.readOnly) {
            return;
        }

        var selection = evt.editor.getSelection().getSelectedElement();
        // Selecting a flash object in editor mode still returns a tag of 'img' (this is because an embedded object has a fake parser element).
        // We only want to work with real elements so we check for the 'data-cke-realelement' attribute (which could represent a fake parser element).
        if (selection && selection.is('img') && !selection.hasAttribute('data-cke-realelement')) {
            if (selection.hasClass(this.name)) {
                return this.setState(CKEDITOR.TRISTATE_ON);
            }
            return this.setState(CKEDITOR.TRISTATE_OFF);
        }
        else {
            return this.setState(CKEDITOR.TRISTATE_DISABLED);
        }
    }
    
    //defining the command executed when the widget is clicked
    function imagehoverCommand(editor, name) {
        this.name = name;
    }
    //Creating a prototype from our command function
    imagehoverCommand.prototype = {
        exec: function (editor) {

            var element = editor.getSelection().getSelectedElement();

            if (!element) {
                return;
            }
            //If there is already a hover shadow class we remove it
            if(this.state === CKEDITOR.TRISTATE_ON){
                element.removeClass('image_hover_shadow');
                return this.setState(CKEDITOR.TRISTATE_OFF);
            //If there isn't a class we add it
            }else if (this.state === CKEDITOR.TRISTATE_OFF) {
                element.addClass('image_hover_shadow');
                return this.setState(CKEDITOR.TRISTATE_ON);
            }
            editor.focus();
            editor.forceNextSelectionCheck();
        }
    };

    CKEDITOR.plugins.add('uw_cee_img_hover', {
        init: function (editor) {
            // Register commands.
            var image_hover_shadow = editor.addCommand('image_hover_shadow', new imagehoverCommand(editor, 'image_hover_shadow'));
            image_hover_shadow.startDisabled = true;

            // Register buttons.
            editor.ui.addButton('Image Hover', {
                label: 'Image Hover',
                command: 'image_hover_shadow',
                icon: this.path + "image_hover_shadow.png"
            });

            // Register state changing handlers.
            editor.on('selectionChange', CKEDITOR.tools.bind(onSelectionChange, image_hover_shadow));
        },
        onLoad: function () {
            CKEDITOR.addCss(
                '.image_hover_shadow {'+
                'box-shadow: 0px 6px 20px rgba(0, 0, 0, 0.60);'+
                '}'
            );
        }

    });

}());
